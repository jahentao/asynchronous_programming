# 异步编程

#### 项目介绍
Java异步编程，学习Demo

#### 软件架构
软件架构说明

一些并发工具/接口的学习使用，基于JDK 8的新特性，所以请设置项目的SDK为JDK 8。

#### 安装教程

1. 参考例子来自[博客](http://colobu.com/2016/02/29/Java-CompletableFuture/)
2. 最好去了解一下Guava项目，其中的Futures。

#### 使用说明

1. 掌握Java异步编程接口和编程模型，首当其冲的是理解**结果传递**的关系。
2. 首先要理解函数式编程的接口：`Consumer`、`Function`、`Supplier`，它们与`Runnable`什么关系？
    1. 像`Consumer`这个接口只有输入，没有返回值。
    2. `Runnable`并不使用`CompletableFuture`计算的结果。
    3. ……注意总结
3. 接着要理解的一个难点是，**异常**在其中的处理方式
4. 总结：可以根据方法的参数的类型来加速你的记忆。
    1. `Runnable`类型的参数会忽略计算的结果，
    2. `Consumer`是纯消费计算结果，
    3. `BiConsumer`会组合另外一个`CompletionStage`纯消费，
    4. `Function`会对计算结果做转换，
    5. `BiFunction`会组合另外一个`CompletionStage`的计算结果做转换。

#### 参考文献

1. [Java 8: Definitive guide to CompletableFuture](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwivnI-BwJ7LAhWpg4MKHRr8CB0QFggcMAA&url=http%3A%2F%2Fwww.nurkiewicz.com%2F2013%2F05%2Fjava-8-definitive-guide-to.html&usg=AFQjCNHxOcm4uRrqZGl1ognxfaTtmB5k5A&sig2=A5rXKfQuabGJMHXAhPUIgA&bvm=bv.115339255,d.eWE)
2. [https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CompletableFuture.html](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CompletableFuture.html)
3. [https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CompletionStage.html](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CompletionStage.html)
#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)