package com.jahentao.demo.async.program;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * thenCompose返回的对象并不一是函数fn返回的对象，
 * 如果原来的CompletableFuture还没有计算出来，
 * 它就会生成一个新的组合后的CompletableFuture。
 * @author jahentao
 * @date 2018/5/31
 * @since 1.0
 */
public class CompletableFutureDemo4 {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            return 100;
        });

        CompletableFuture<String> f =  future.thenCompose( i -> {
            return CompletableFuture.supplyAsync(() -> {
                return (i * 10) + "";
            });
        });

        System.out.println(f.get());
    }
}
/*
输出结果：
1000
 */