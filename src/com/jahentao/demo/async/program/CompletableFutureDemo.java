package com.jahentao.demo.async.program;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * 在Java 8中, 新增加了一个包含50个方法左右的类: CompletableFuture，
 * 提供了非常强大的Future的扩展功能，
 * 可以帮助我们简化异步编程的复杂性，提供了函数式编程的能力，
 * 可以通过回调的方式处理计算结果，并且提供了转换和组合CompletableFuture的方法。
 *
 * <p>很多语言，比如Node.js，采用回调的方式实现异步编程。</p>
 * <p>Java的一些框架，比如Netty，自己扩展了Java的 Future接口，提供了addListener等多个扩展方法：</p>
 * <p>其他，Google guava也提供了通用的扩展Future:ListenableFuture、SettableFuture 以及辅助类Futures等,方便异步编程。</p>
 * @author jahentao
 * @date 2018/5/31
 * @since 1.0
 */
public class CompletableFutureDemo {

    public static CompletableFuture<Integer> compute() {
        final CompletableFuture<Integer> future = new CompletableFuture<>();
        return future;
    }

    public static void main(String[] args) throws Exception {
        final CompletableFuture<Integer> f = compute();
        class Client extends Thread {
            CompletableFuture<Integer> f;
            Client(String threadName, CompletableFuture<Integer> f) {
                super(threadName);
                this.f = f;
            }
            @Override
            public void run() {
                try {
                    System.out.println(this.getName() + ": " + f.get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }
        new Client("Client1", f).start();
        new Client("Client2", f).start();
        System.out.println("waiting");
        f.complete(100);
        //f.completeExceptionally(new Exception());
//        System.in.read(); // 其他操作
    }
}
/*
输出结果：
waiting
Client1: 100
Client2: 100
*/